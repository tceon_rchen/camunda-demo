package com.example.controller;

import camundajar.impl.com.google.gson.JsonObject;
import com.alibaba.fastjson.JSONObject;
import org.camunda.bpm.engine.RepositoryService;
import org.camunda.bpm.engine.RuntimeService;
import org.camunda.bpm.engine.TaskService;
import org.camunda.bpm.engine.runtime.ProcessInstance;
import org.camunda.bpm.engine.runtime.ProcessInstantiationBuilder;
import org.camunda.bpm.engine.task.Task;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import java.util.HashMap;
import java.util.List;
import java.util.UUID;

@RestController
@RequestMapping("process1")
public class Process1Controller {
    private Logger logger = LoggerFactory.getLogger(Process1Controller.class.getName());

    @Autowired
    private RepositoryService repositoryService;
    @Autowired
    private RuntimeService runtimeService;
    @Autowired
    private TaskService taskService;

    @Value("${process.student.key}")
    String key;

    @RequestMapping("startProcessInstance")
    public void startProcessInstance(){

        String uuid = UUID.randomUUID().toString().replace("-", "");
        ProcessInstantiationBuilder processInstanceBuilder =
                runtimeService.createProcessInstanceByKey(key);
        ProcessInstance processInstance =
                processInstanceBuilder
                        .businessKey(uuid)
                        .execute();

        logger.info("流程部署成功：id:{}  ",processInstance.getId());
    }

    /**
     * 根据经办人查询任务，查出的是所有当前人员下面所有的任务，
     * 如果同时存在多个流程定义（请假，报销等），启动了不同的流程实例，
     * 都能查到，可以添加 processDefinitionKey()等查询条件
     * @param userName
     */
    @RequestMapping("queryTask")
    public void queryTask(String userName) {
        //该 List<Task> list 不能作为返回值，也不能作为value通过JSONObject传输，需要自定义对象返回
        List<Task> list = taskService.createTaskQuery().list();
        List<Task> list0 = taskService.createTaskQuery().taskAssignee("张老师").list();
        List<Task> list1 = taskService.createTaskQuery().taskCandidateUser("张老师").list();
        List<Task> list2 = taskService.createTaskQuery().taskCandidateUser("王老师").list();
        List<Task> list3 = taskService.createTaskQuery().taskCandidateUser("李老师").list();


        logger.info("任务列表：{}",list);
//        JSONObject jsonObject = new JSONObject();
//        jsonObject.put("data",list);

    }

    @RequestMapping("complete")
    public void complete(String taskId,boolean audit){
        HashMap<String, Object> map = new HashMap<>();
        map.put("audit",audit);
        taskService.createComment("","","");
        taskService.complete(taskId,map);
        logger.info("处理任务成功！任务id{}" ,taskId);
    }
    @RequestMapping("setAssignee")
    public void setAssignee(String taskId,String userName){
        taskService.setAssignee(taskId,userName);
        logger.info("设置经办人，任务id{},经办人{}" ,taskId,userName);
    }

    @RequestMapping("claim")
    public void claim(String taskId,String userName){
        taskService.claim(taskId,userName);
    }

    @RequestMapping("backToCandidate")
    public void backToCandidate(String taskId){
       taskService.setAssignee(taskId,null);
    }


}
