package com.example.event;

import org.camunda.bpm.engine.delegate.DelegateTask;
import org.camunda.bpm.engine.delegate.TaskListener;
import org.camunda.bpm.model.bpmn.BpmnModelInstance;
import org.camunda.bpm.model.bpmn.instance.camunda.CamundaProperties;
import org.camunda.bpm.model.bpmn.instance.camunda.CamundaProperty;
import java.util.Collection;

import java.util.Map;

public class ExtensionListener implements TaskListener {
    @Override
    public void notify(DelegateTask delegateTask) {

        String id = delegateTask.getId();
        String assignee = delegateTask.getAssignee();
        //获取Input/Output值以及其他变量
        Map<String, Object> variables = delegateTask.getVariables();

        //获取Extensions中的properties属性key - value
        BpmnModelInstance bpmnModel = delegateTask.getBpmnModelInstance();
        Collection<CamundaProperties> camundaPropertiesCollection = bpmnModel.getModelElementsByType(CamundaProperties.class);
        for (CamundaProperties camundaProperties : camundaPropertiesCollection) {
            Collection<CamundaProperty> camundaProperty = camundaProperties.getCamundaProperties();
            for (CamundaProperty property : camundaProperty) {
                String propertyName = property.getCamundaName();
                String propertyValue = property.getCamundaValue();
                System.out.println("============");
            }

        }
        System.out.println("========================================");
    }
}
