package com.example.event;

import org.camunda.bpm.engine.TaskService;
import org.camunda.bpm.engine.delegate.DelegateTask;
import org.camunda.bpm.engine.delegate.TaskListener;
import org.camunda.bpm.engine.task.Task;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import java.security.Key;

@Component
public class Process1Event implements TaskListener {

    @Autowired
    TaskService taskService;

    @Override
    public void notify(DelegateTask delegateTask) {
        String taskId = delegateTask.getId();
        String currAssignee = delegateTask.getAssignee();
        String users = "";
        String nextAssignee1 = "";
        if(null != currAssignee && !"".equals(currAssignee)){
            //可以获取当前任务的经办人，查询接下来的经办人
            //do
            users = "张老师,王老师,李老师,刘老师";
            nextAssignee1 = "王主任";
        }
        delegateTask.setVariable("users",users);
        delegateTask.setVariable("assignee1",nextAssignee1);
    }
}
