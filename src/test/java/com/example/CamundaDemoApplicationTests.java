package com.example;


import org.camunda.bpm.engine.*;
import org.camunda.bpm.engine.history.HistoricActivityInstance;
import org.camunda.bpm.engine.history.HistoricActivityInstanceQuery;
import org.camunda.bpm.engine.repository.Deployment;
import org.camunda.bpm.engine.repository.ProcessDefinition;
import org.camunda.bpm.engine.runtime.ProcessInstance;
import org.camunda.bpm.engine.runtime.ProcessInstantiationBuilder;
import org.camunda.bpm.engine.task.Task;
import org.camunda.bpm.model.bpmn.BpmnModelInstance;
import org.camunda.bpm.model.bpmn.instance.UserTask;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;

import java.util.Collection;
import java.util.HashMap;
import java.util.List;

@SpringBootTest
class CamundaDemoApplicationTests {


    @Autowired
    RuntimeService runtimeService;
    @Autowired
    TaskService taskService;
    @Autowired
    IdentityService identityService;
    @Autowired
    FormService formService;
    @Autowired
    HistoryService historyService;
    @Autowired
    ManagementService managementService;
    @Autowired
    FilterService filterService;
    @Autowired
    ExternalTaskService externalTaskService;
    @Autowired
    CaseService caseService;
    @Autowired
    DecisionService decisionService;
    @Autowired
    RepositoryService repositoryService;

    /**
     * 部署流程
     */
    @Test
    void deployment() {
        Deployment deployment = repositoryService.createDeployment()
                .name("diagram02部署测试")
                .addClasspathResource("processes/diagram02.bpmn")
                .deploy();
        String id = deployment.getId();
        String name = deployment.getName();
        System.out.println("部署成功：名称：" + name + ",id:" + id);
    }

    /**
     * 查看流程部署
     */
    @Test
    void deploymentList() {
//可以根据不同的条件查询
        List<Deployment> list = repositoryService.createDeploymentQuery().orderByDeploymentId().desc().list();
        System.out.println(list);
    }

    @Test
    void startProcessInstance() {
        ProcessInstantiationBuilder processInstanceBuilder =
                runtimeService.createProcessInstanceByKey("Process_qj");
        ProcessInstance processInstance =
                processInstanceBuilder
                        .businessKey("qjUUID1")
                        .execute();
        System.out.println("流程实例id : " + processInstance.getId());
    }

    /**
     * 根据流程实例id查询任务实例（部署的同一个流程，每次发起都是一个新的流程实例）
     * 还可添加其他的查询条件
     */
    @Test
    void queryTask() {
        List<Task> list = taskService.createTaskQuery().list();
        System.out.println(list);
    }


    @Test
    void setAssignee(){
        taskService.setAssignee("416429aa-6715-11ec-b934-502b73a0091a","lisi");
    }

    @Test
    void com(){
        HashMap<String, Object> map = new HashMap<>();
        map.put("mapKey","mapValue");
        taskService.complete("311f0303-6716-11ec-8dd3-502b73a0091a",map);

    }
    @Test
    void getHis(){
        List<HistoricActivityInstance> list = historyService.createHistoricActivityInstanceQuery().processInstanceId("b806a73b-67e8-11ec-b01e-502b73a0091a")
                .orderByHistoricActivityInstanceStartTime().desc().list();
        System.out.println(list);
        List<Task> list1 = taskService.createTaskQuery().processInstanceBusinessKey("7ef23782fce748d78fcf44e597cb544a").list();
        System.out.println("================");


    }
    @Test
    void getNodeList(){
//        List<ProcessDefinition> list = repositoryService.createProcessDefinitionQuery().processDefinitionKey("Process_student").list();
//        System.out.println("====================");

        List<ProcessDefinition> process_student = repositoryService.createProcessDefinitionQuery()
                .processDefinitionKey("Process_student")
                .orderByVersionTag().desc()
                .list();
        //流程已经存在，需要查询当前流程版本对应的节点
        //List<Task> taskList = taskService.createTaskQuery().processInstanceBusinessKey("7ef23782fce748d78fcf44e597cb544a").list();
        //String definitionId = taskList.get(0).getProcessDefinitionId();
        //流程未发起，查询最新版本的节点
        String definitionId = process_student.get(0).getId();
        BpmnModelInstance modelInstance = repositoryService.getBpmnModelInstance(definitionId);
        Collection<UserTask> modelElementsByType = modelInstance.getModelElementsByType(UserTask.class);
        for (UserTask userTask : modelElementsByType) {
            System.out.println(userTask.getName());
            System.out.println(userTask.getId());
            System.out.println(userTask.getCamundaAssignee());
            System.out.println(userTask.getCamundaCandidateUsers());

        }

    }
    @Test
    void getFinishedNode(){
        Task task = taskService.createTaskQuery().taskId("6af23bac-67ee-11ec-9f9b-502b73a0091a").singleResult();

        List<HistoricActivityInstance> list = historyService
                .createHistoricActivityInstanceQuery()
                .processInstanceId(task.getProcessInstanceId())
                .orderByHistoricActivityInstanceStartTime().desc()
                .finished().list();
        for (HistoricActivityInstance instance : list) {
            System.out.println(instance.getActivityName());
            System.out.println(instance.getAssignee());
            System.out.println(instance.getActivityType());
            System.out.println("---------------------------");
        }
    }

}
